# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Basic functionality for interacting with cryptocurrencies online exchanges APIs: stats and online rates for arbitrage purposes

### How do I get set up? ###

Just run `PYTHONPATH=. python trade_machine/scripts/search_arbitrage.py`

* Requirements: `requests`, `pymongo`

### Contribution guidelines ###

Code mostly writen under pep-8
