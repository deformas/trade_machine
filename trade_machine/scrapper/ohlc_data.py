# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import datetime as dt

class OHLCData(object):
    """Provides class to store and combine OHLCData
    """
    
    def __init__(self):
       self.close = 0
       self.open = 0
       self.low = 0
       self.high = 0
       self.volume = 0
       self.time = None
        
    def to_db(self):
        return {
            'close': self.close,
            'open': self.open,
            'low': self.low,
            'high': self.high,
            'volume': self.volume,
            'time': self.time
        }
        
    def to_dict(self):
        return {
            'close': self.close,
            'open': self.open,
            'low': self.low,
            'high': self.high,
            'volume': self.volume,
            'time': self.time.strftime('%Y-%m-%d %H-%M-%S%z')
        }
        
    def __str__(self):
        return json.dumps(self.to_dict())