# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

from trade_machine.scrapper import apis

class DataSource(object):
    Sources = \
    \
    (Cryptocompare, ) = \
    ('cryptocompare', )

    @classmethod
    def get_api(cls, source):
        if source not in cls.Sources:
            raise ValueError("{} not in available sources".format(source))
        else:
            if source == cls.Cryptocompare:
                return apis.CryptocompareApi()

    @classmethod
    def is_available(cls, source):
        return source in cls.Sources