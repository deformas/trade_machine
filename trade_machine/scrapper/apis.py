# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import requests
import datetime
import logging
import time

from trade_machine.common import time_utils
from trade_machine.scrapper import ohlc_data

WAIT_FOR_NEW_REQ_SEC = 1

logging.basicConfig(level=logging.INFO)

class CryptocompareApi(object):
    """ APIs for loading data from cryptocompare.com 
    """
    URL = 'https://min-api.cryptocompare.com/data/'

    ExchangeList = \
    (ExKraken, ExBitstamp, ExYobit, ExPoloniex, ExBittrex, ExHitBTC) = \
    ('kraken', 'bitstamp', 'yobit', 'poloniex', 'bittrex', 'hitbtc')

    DataFrequency = \
    (FreqMinutely, FreqHourly, FreqDaily) = \
    ('minutely', 'hourly', 'daily')

    Methods = \
    (MethodHistoMinute, MethodHistoHour, MethodHistoDay) = \
    ('histominute', 'histohour', 'histoday')

    Currencies = \
    (CurBTC, CurUSD) = \
    ('btc', 'usd')

    GetLimit = 1000

    def __init__(self):
        pass

    def _method_for_freq(self, freq):
        if freq == self.FreqMinutely:
            return self.MethodHistoMinute
        if freq == self.FreqHourly:
            return self.MethodHistoHour
        if freq == self.FreqDaily:
            return self.MethodHistoDay
        raise ValueError("{} freq is not implemented")

    def _get_currency_data(self, cur_from, cur_to, to_ts, exchange, freq):
        params = {
            'fsym': cur_from,
            'tsym': cur_to,
            'limit': 1000,
            'aggregate': 1,
            'toTs': to_ts,
            'e': exchange
        }
        url = self.URL + self._method_for_freq(freq)
        r = requests.get(url, params=params)
        return r.json()

    def _to_ohlc(self, item):
        ohlc = ohlc_data.OHLCData()
        ohlc.close = item['close']
        ohlc.open = item['open']
        ohlc.low = item['low']
        ohlc.high = item['high']
        ohlc.volume = item['volumefrom']
        ohlc.time = datetime.datetime.fromtimestamp(item['time'])

        return ohlc

    def get_currency_history(self, cur_from, cur_to, exchange, freq, dt_from):
        add_ts = 0
        if freq == self.FreqHourly:
            add_ts = datetime.timedelta(hours=1)
        elif freq == self.FreqDaily:
            add_ts = datetime.timedelta(days=1)
        elif freq == self.FreqMinutely:
            add_ts = datetime.timedelta(minutes=1)
        else:
            raise ValueError("Frequency {} is not available".format(freq))

        
        data = dict()
        one_more = True
        while dt_from < datetime.datetime.now() or one_more:
            new_data = self._get_currency_data(cur_from, cur_to, int(dt_from.timestamp()), exchange, freq)
            if new_data['Response'] == 'Error' and freq == self.FreqMinutely:
                dt_from += add_ts * self.GetLimit
                time.sleep(WAIT_FOR_NEW_REQ_SEC)
                logging.info('Started from too early')
                continue

            if new_data['Response'] == 'Error':
                raise Exception(new_data)

            for item in new_data['Data']:
                # not fully observed data
                if datetime.datetime.fromtimestamp(item['time']) + add_ts < datetime.datetime.now():
                    data[item['time']] = self._to_ohlc(item)
                # else:
                #     logging.info(datetime.datetime.fromtimestamp(item['time']))

                dt_from = max(datetime.datetime.fromtimestamp(item['time']), dt_from)
            logging.info('Loaded {0} new elements from {1} to {2}'.format(
                len(new_data['Data']),
                time_utils.ts2str(new_data['TimeFrom']),
                time_utils.ts2str(new_data['TimeTo'])
            ))
            if dt_from > datetime.datetime.now():
                one_more = False
            dt_from += add_ts * self.GetLimit

            time.sleep(WAIT_FOR_NEW_REQ_SEC)

        return data
