# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

class Order(object):
    BID = 'bid'
    ASK = 'ask'

    def __init__(self, price, volume, type):
        self.price = price
        self.volume = volume
        self.type = type

    def __repr__(self):
        return '<Order price={:.6f}, volume={:.6f}>'.format(
            self.price,
            self.volume
        )

class OrderBook(object):
    def __init__(self, ask_orders, bid_orders, check_sort=False):
        self.ask_orders = ask_orders
        self.bid_orders = bid_orders

        if check_sort:
            if not self._is_sorted(self.ask_orders, inverse=True):
                self.ask_orders = sorted(self.ask_orders, key=lambda x: x.price)
        
            if self._is_sorted(self.bid_orders, inverse=True):
                self.bid_orders = sorted(self.bid_orders, key=lambda x: -x.price)

    def top_n(self, n, type):
        if type == Order.BID:
            return self.bid_orders[:n]
        elif type == Order.ASK:
            return self.ask_orders[:n]
        else:
            raise ValueError("Unrecognized order type")

    def _is_sorted(self, orders, inverse=False):
        return all(a.price <= b.price for a, b in zip(orders[:-1], orders[1:]))

    def __repr__(self):
        return '<OrderBook asks={} bids={}>'.format(
            ', '.join(str(a) for a in self.top_n(5, 'ask')),
            ', '.join(str(b) for b in self.top_n(5, 'bid'))
        )

