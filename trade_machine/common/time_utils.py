# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import datetime

class TimeConstants:
    DAY = datetime.timedelta(days=1).total_seconds()
    HOUR = datetime.timedelta(hours=1).total_seconds()
    MINUTE = datetime.timedelta(minutes=1).total_seconds()
    SECOND = datetime.timedelta(seconds=1).total_seconds()

def ts_now():
    return datetime.datetime.now().timestamp()

def ts2str(ts):
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H-%M-%S%z')