# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

from trade_machine.common import currencies
from trade_machine.common import order_book

import requests

TIMEOUT_SEC = 3

class NoPairException(Exception):
    def __init__(self, cur_from, cur_to):
        super(NoPairException, self).__init__('No pair for {} / {}'.format(
            currencies.TRADE_NAMES[cur_from],
            currencies.TRADE_NAMES[cur_to]
        ))

class NoCurrencyException(Exception):
    def __init__(self, cur):
        super(NoPairException, self).__init__('No currency {}'.format(
            currencies.TRADE_NAMES[cur]
        ))

class _AbstractExchange(object):
    TX_TYPES = \
    (TX_TAKER, TX_MAKER) = \
    ('Taker', 'Maker')

    def __init__(self, name, tx_fee_taker, tx_fee_maker, deposit_cur, withdraw_cur):
        self.name = name
        self.tx_fee_maker = tx_fee_maker
        self.tx_fee_taker = tx_fee_taker
        self.deposit_cur = deposit_cur
        self.withdraw_cur = withdraw_cur

    def get_name(self):
        return self.name

    def get_depth(self, cur_from, cur_to, depth_size):
        raise NotImplementedError()

    def get_tx_fee(self, price, amount, tx_type=TX_TAKER):
        if tx_type == TX_TAKER:
            return self.tx_fee_taker * amount * price
        elif tx_type == TX_MAKER:
            return self.tx_fee_maker * amount * price
        else:
            raise ValueError("Type {} of transactions is not supported".format(tx_type))

    def get_deposit_fee(self, amount, cur):
        if cur in self.deposit_cur:
            return self.deposit_cur[cur][0] * amount + self.deposit_cur[cur][1]
        else:
            if currencies.is_fiat(cur) and currencies.Currency.ANY_FIAT in self.deposit_cur:
                return self.deposit_cur[currencies.Currency.ANY_FIAT][0] * amount + self.deposit_cur[currencies.Currency.ANY_FIAT]
            if not currencies.is_fiat(cur) and currencies.Currency.ANY_CRYPTO in self.deposit_cur:
                return self.deposit_cur[currencies.Currency.ANY_CRYPTO][0] * amount + self.deposit_cur[currencies.Currency.ANY_CRYPTO]
            if currencies.Currency.ANY in self.deposit_cur:
                return self.deposit_cur[currencies.Currency.ANY][0] * amount + self.deposit_cur[currencies.Currency.ANY]

            raise NoCurrencyException(cur)

    def get_withdrawal_fee(self, amount, cur):
        if cur in self.withdraw_cur:
            return self.withdraw_cur[cur][0] * amount + self.withdraw_cur[cur][1]
        else:
            if currencies.is_fiat(cur) and currencies.Currency.ANY_FIAT in self.withdraw_cur:
                return self.withdraw_cur[currencies.Currency.ANY_FIAT][0] * amount + self.withdraw_cur[currencies.Currency.ANY_FIAT]
            if not currencies.is_fiat(cur) and currencies.Currency.ANY_CRYPTO in self.withdraw_cur:
                return self.withdraw_cur[currencies.Currency.ANY_CRYPTO][0] * amount + self.withdraw_cur[currencies.Currency.ANY_CRYPTO]
            if currencies.Currency.ANY in self.withdraw_cur:
                return self.withdraw_cur[currencies.Currency.ANY][0] * amount + self.withdraw_cur[currencies.Currency.ANY]

            raise NoCurrencyException(cur)

class KrakenExchange(_AbstractExchange):
    """kraken.com cryptocurrency exchange
    """
    AvailablePairs = set(
        [(currencies.Currency.USDollar, currencies.Currency.Bitcoin)]
    )

    def __init__(self):
        super(KrakenExchange, self).__init__('Kraken')

    @staticmethod
    def _kraken_pair(cur_from, cur_to):
        result = ''
        for c in (cur_from, cur_to):
            if currencies.is_fiat(c):
                result += 'x'
            else:
                result += 'z'
            result += currencies.ISO_NAMES[c]

        return result.to_upper()

    @classmethod
    def get_pair(cls, cur_from, cur_to):
        if (cur_from, cur_to) in cls.AvailablePairs:
            return cls._kraken_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):
        pass


class YobitExchange(_AbstractExchange):

    BASE_URL = 'https://yobit.net/api/3/'
    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set([
        currencies.Currency.BTS,
        currencies.Currency.OMG,
        currencies.Currency.BCD,
        currencies.Currency.BNB,
        currencies.Currency.MUE,
        currencies.Currency.REP,
        currencies.Currency.DCT,
        currencies.Currency.PAY,
        currencies.Currency.EXP

    ])

    def __init__(self, names):
        super(YobitExchange, self).__init__(
            'Yobit',
            tx_fee_taker=0.2 * 0.01,
            tx_fee_maker=0.2 * 0.01,
            deposit_cur={
                currencies.Currency.ANY_CRYPTO: (0.0, 0.0),
                currencies.Currency.RussianRuble: (0.06, 0.0),
                currencies.Currency.USDollar: (0.03, 0.0)
            },
            withdraw_cur={
                currencies.Currency.ANY_CRYPTO: (0.0, 0.0005),
                currencies.Currency.RussianRuble: (0.02, 0.0),
                currencies.Currency.USDollar: (0.03, 0.0)
            }
        )

        self.names = dict(names)
        self.names[currencies.Currency.RipioCredit] = "RipioCredit"
        self.available_pairs = self._get_available_pairs()

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'info')
        r.raise_for_status()

        pairs = r.json()['pairs']
        result = set()

        for k, v in pairs.items():
            cur_from_id, cur_to_id = k.split('_')
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _yobit_pair(self, cur_from, cur_to):
        return '{}_{}'.format(
            self.names[cur_from].lower(),
            self.names[cur_to].lower()
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._yobit_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):
        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'depth/' + pair_name
        params = {
            'limit': depth_size
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        result = r.json()[pair_name]
        asks = [order_book.Order(price=r[0], volume=r[1], type='ask') for r in result.get("asks", [])]
        bids = [order_book.Order(price=r[0], volume=r[1], type='bid') for r in result.get("bids", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class BitfinexExchange(_AbstractExchange):

    BASE_URL = "https://api.bitfinex.com/v1/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):
        super(BitfinexExchange, self).__init__(
            'Bitfinex',
            tx_fee_taker=0.2 * 0.01,
            tx_fee_maker=0.1 * 0.01,
            deposit_cur={
                currencies.Currency.Bitcoin: (0.0, 0.0005),
                currencies.Currency.Iota: (0.0, 0.5),
                currencies.Currency.Litecoin: (0.0, 0.001),
                currencies.Currency.Ethereum: (0.0, 0.01),
                currencies.Currency.BitcoinCash: (0.0, 0.0001),
                currencies.Currency.EOS: (0.0, 0.1),
                currencies.Currency.Monero: (0.0, 0.04),
                currencies.Currency.EthereumClassic: (0.0, 0.01),
                currencies.Currency.DASH: (0.0, 0.01),
                currencies.Currency.Ripple: (0.0, 0.02),
                currencies.Currency.SantimentToken: (0.0, 0.1),
                currencies.Currency.ZCash: (0.0, 0.001),
                currencies.Currency.BitcoinGold: (0.0, 0.0),
                currencies.Currency.NEO: (0.0, 0.0),
                currencies.Currency.OmiseGO: (0.0, 0.1),
                currencies.Currency.Tether: (0.0, 0.0),
                currencies.Currency.USDollar: (0.001, 20.0)
            },
            withdraw_cur={
                currencies.Currency.Bitcoin: (0.0, 0.0005),
                currencies.Currency.Iota: (0.0, 0.5),
                currencies.Currency.Litecoin: (0.0, 0.001),
                currencies.Currency.Ethereum: (0.0, 0.01),
                currencies.Currency.BitcoinCash: (0.0, 0.0001),
                currencies.Currency.EOS: (0.0, 0.1),
                currencies.Currency.Monero: (0.0, 0.04),
                currencies.Currency.EthereumClassic: (0.0, 0.01),
                currencies.Currency.DASH: (0.0, 0.01),
                currencies.Currency.Ripple: (0.0, 0.02),
                currencies.Currency.SantimentToken: (0.0, 0.1),
                currencies.Currency.ZCash: (0.0, 0.001),
                currencies.Currency.BitcoinGold: (0.0, 0.0),
                currencies.Currency.NEO: (0.0, 0.0),
                currencies.Currency.OmiseGO: (0.0, 0.1),
                currencies.Currency.Tether: (0.0, 0.0),
                currencies.Currency.USDollar: (0.001, 20.0)
            }
        )

        self.names = dict(names)
        # BitConnectCoin
        # Bitcoin Cash
        self.names[currencies.Currency.BitcoinCash] = 'BCH'
        self.names[currencies.Currency.QTM] = 'Quantum'
        self.names[currencies.Currency.QTUM] = 'QTM'

        self.available_pairs = self._get_available_pairs()
        self.fee = 0.2 * 0.01

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'symbols')
        r.raise_for_status()

        pairs = r.json()
        result = set()

        for k in pairs:
            cur_from_id, cur_to_id = k[:3], k[3:]
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _bitfinex_pair(self, cur_from, cur_to):
        return '{}{}'.format(
            self.names[cur_from].lower(),
            self.names[cur_to].lower()
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._bitfinex_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):
        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'book/' + pair_name
        params = {
            'limit_bids': depth_size,
            'limit_asks': depth_size
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        result = r.json()
        # print(result)
        asks = [order_book.Order(price=float(r['price']), volume=float(r['amount']), type='ask') for r in result["asks"]]
        bids = [order_book.Order(price=float(r['price']), volume=float(r['amount']), type='bid') for r in result["bids"]]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class BittrexExchange(_AbstractExchange):

    BASE_URL = "https://bittrex.com/api/v1.1/public/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):

        super(BittrexExchange, self).__init__(
            'Bittrex',
            tx_fee_taker=0.25 * 0.01,
            tx_fee_maker=0.25 * 0.01,
            deposit_cur={
                currencies.Currency.ANY_CRYPTO: (0.0, 0.0),
            },
            withdraw_cur={
                currencies.Currency.ANY_CRYPTO: (0.0, 0.001),
                currencies.Currency.Tether: (0.0, 5.0)
            }
        )

        self.names = dict(names)

        # raise ValueError("Names was not checked yet, aborted")
        self.available_pairs = self._get_available_pairs()
        # self.fee = 0.25 * 0.01

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'getmarkets')
        r.raise_for_status()

        pairs = r.json()['result']
        result = set()

        for k in pairs:
            cur_from_id, cur_to_id = k['BaseCurrency'], k['MarketCurrency']
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _bittrex_pair(self, cur_from, cur_to):
        return '{}-{}'.format(
            self.names[cur_from],
            self.names[cur_to]
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._bittrex_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):
        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'getorderbook'
        params = {
            'market': pair_name,
            'type': 'both'
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        result = r.json()['result']
        # print(result)
        asks = [order_book.Order(price=float(r['Rate']), volume=float(r['Quantity']), type='ask') for r in result["sell"]]
        bids = [order_book.Order(price=float(r['Rate']), volume=float(r['Quantity']), type='bid') for r in result["buy"]]

        # limit them
        asks = asks[:min(depth_size, len(asks))]
        bids = bids[:min(depth_size, len(bids))]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class BinanceExchange(_AbstractExchange):

    BASE_URL = "https://www.binance.com/api/v1/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):

        super(BinanceExchange, self).__init__(
            'Binance',
            tx_fee_maker=1.0,
            tx_fee_taker=1.0,
            deposit_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0)
            },
            withdraw_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0)
            }
        )

        self.names = dict(names)
        self.names[currencies.Currency.PowerLedger] = 'POWR'

        self.available_pairs = self._get_available_pairs()


    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'ticker/allPrices')
        r.raise_for_status()

        pairs = r.json()
        result = set()

        for k in pairs:
            cur_from_id, cur_to_id = k['symbol'][:3], k['symbol'][3:]
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _binance_pair(self, cur_from, cur_to):
        return '{}{}'.format(
            self.names[cur_from],
            self.names[cur_to]
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._binance_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):
        if depth_size < 5:
            depth_size = 5

        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'depth'
        params = {
            'symbol': pair_name,
            'limit': depth_size
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        result = r.json()
        asks = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='ask') for r in result.get("asks", [])]
        bids = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='bid') for r in result.get("bids", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class PoloniexExchange(_AbstractExchange):

    BASE_URL = "https://poloniex.com/public"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):

        # raise ValueError("Names was not checked yet")

        params = {
            'command': 'returnCurrencies'
        }
        r = requests.get(self.BASE_URL, params)
        r.raise_for_status()

        self.names = dict(names)
        self.names[currencies.Currency.BitcoinCash] = 'BCH'

        curs = r.json()
        deposit_cur = dict()
        withdraw_cur = dict()

        for ticker, data in curs.items():
            deposit_cur[currencies.inverse_search(self.names, ticker)] = (0, 0)
            withdraw_cur[currencies.inverse_search(self.names, ticker)] = (0, float(data['txFee']))


        super(PoloniexExchange, self).__init__(
            "Poloniex",
            tx_fee_taker=0.25 * 0.01,
            tx_fee_maker=0.15 * 0.01,
            deposit_cur=deposit_cur,
            withdraw_cur=withdraw_cur
        )

        self.available_pairs = self._get_available_pairs()

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL, {'command': 'returnTicker'})
        r.raise_for_status()

        pairs = r.json()
        result = set()

        for k, v in pairs.items():
            cur_from_id, cur_to_id = k[:3], k[4:]
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if int(v['isFrozen']) == 1:
                continue

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _poloniex_pair(self, cur_from, cur_to):
        return '{}_{}'.format(
            self.names[cur_from],
            self.names[cur_to]
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._poloniex_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):

        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL
        params = {
            'command': 'returnOrderBook',
            'currencyPair': pair_name,
            'depth': depth_size
        }
        r = requests.get(url, params, timeout=TIMEOUT_SEC)
        r.raise_for_status()

        result = r.json()
        # print(result)
        asks = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='ask') for r in result.get("asks", [])]
        bids = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='bid') for r in result.get("bids", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class HitBTCExchange(_AbstractExchange):

    BASE_URL = "https://api.hitbtc.com/api/2/public/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):

        # raise ValueError("Names was not checked yet")

        r = requests.get(self.BASE_URL + 'currency')
        r.raise_for_status()

        self.names = dict(names)
        self.names[currencies.Currency.BitcoinCash] = 'BCH'
        self.names[currencies.Currency.BitConnect] = 'BCC'
        self.names[currencies.Currency.Bitmark] = 'Bitmark'

        curs = r.json()
        deposit_cur = dict()
        withdraw_cur = dict()

        for data in curs:
            if not data['transferEnabled'] or not data['payinEnabled'] or not data['payoutEnabled']:
                continue

            deposit_cur[currencies.inverse_search(self.names, data['id'])] = (0, 0)
            withdraw_cur[currencies.inverse_search(self.names, data['id'])] = (0, 0.01)

        withdraw_cur[currencies.Currency.Bitcoin] = (0, 0.00085)
        withdraw_cur[currencies.Currency.Litecoin] = (0, 0.001)
        withdraw_cur[currencies.Currency.DOGE] = (0, 1.0)
        withdraw_cur[currencies.Currency.NXT] = (0, 1.0)
        withdraw_cur[currencies.Currency.BCN] = (0, 0.01)
        withdraw_cur[currencies.Currency.NBT] = (0, 0.02)
        withdraw_cur[currencies.Currency.XDN] = (0, 0.001)
        withdraw_cur[currencies.Currency.XMR] = (0, 0.1)
        withdraw_cur[currencies.Currency.LSK] = (0, 0.1)
        withdraw_cur[currencies.Currency.STEEM] = (0, 0.1)
        withdraw_cur[currencies.Currency.SBD] = (0, 0.01)
        withdraw_cur[currencies.Currency.SC] = (0, 10.0)
        withdraw_cur[currencies.Currency.ARDR] = (0, 1.0)
        withdraw_cur[currencies.Currency.ZEC] = (0, 0.0001)

        super(HitBTCExchange, self).__init__(
            "HitBTC",
            tx_fee_taker=0.1 * 0.01,
            tx_fee_maker=-0.01 * 0.01, #yep, it's even negative
            deposit_cur=deposit_cur,
            withdraw_cur=withdraw_cur
        )

        self.available_pairs = self._get_available_pairs()

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'symbol')
        r.raise_for_status()

        pairs = r.json()
        result = set()

        for p in pairs:
            cur_from_id, cur_to_id = p['id'][:3], p['id'][3:]
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _hitbtc_pair(self, cur_from, cur_to):
        return '{}{}'.format(
            self.names[cur_from],
            self.names[cur_to]
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._hitbtc_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):

        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'orderbook/' + pair_name
        params = {
            'limit': depth_size
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        result = r.json()
        asks = [order_book.Order(price=float(r['price']), volume=float(r['size']), type='ask') for r in result.get("ask", [])]
        bids = [order_book.Order(price=float(r['price']), volume=float(r['size']), type='bid') for r in result.get("bid", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)

class LiquiExchange(_AbstractExchange):
    BASE_URL = "https://api.liqui.io/api/3/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):

        super(LiquiExchange, self).__init__(
            'Liqui.io',
            tx_fee_maker=0.25 * 0.01,
            tx_fee_taker=0.25 * 0.01,
            deposit_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0)
            },
            withdraw_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0.001)
            }
        )

        self.names = dict(names)

        self.available_pairs = self._get_available_pairs()

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'info')
        r.raise_for_status()

        pairs = r.json()['pairs']
        result = set()

        for key, value in pairs.items():
            if value['hidden'] != 0:
                continue

            cur_from_id, cur_to_id = key.split('_')
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _liqui_pair(self, cur_from, cur_to):
        return '{}_{}'.format(
            self.names[cur_from].lower(),
            self.names[cur_to].lower()
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._liqui_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):

        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'depth/' + pair_name
        params = {
            'limit': depth_size
        }
        try:
            r = requests.get(url, params, timeout=TIMEOUT_SEC)
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        r.raise_for_status()

        try:
            result = r.json()[pair_name]
        except Exception as ex:
            print(ex)
            return order_book.OrderBook(ask_orders=[], bid_orders=[])

        asks = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='ask') for r in result.get("asks", [])]
        bids = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='bid') for r in result.get("bids", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)


class EXXExchange(_AbstractExchange):
    BASE_URL = "https://api.exx.com/data/v1/"

    TIME_PER_REQ_SEC = 1

    NOT_WITHDRAWABLE = set()

    def __init__(self, names):
        super(EXXExchange, self).__init__(
            'EXX',
            tx_fee_maker=0.1 * 0.01,
            tx_fee_taker=0.1 * 0.01,
            deposit_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0)
            },
            withdraw_cur={
                currencies.Currency.ANY_CRYPTO: (0, 0.001)
            }
        )

        self.names = dict(names)

        self.available_pairs = self._get_available_pairs()

    def _get_available_pairs(self):
        r = requests.get(self.BASE_URL + 'markets')
        r.raise_for_status()

        pairs = r.json()
        result = set()

        for key, value in pairs.items():
            if not value['isOpen']:
                continue

            cur_from_id, cur_to_id = key.split('_')
            cur_from = currencies.inverse_search(self.names, cur_from_id)
            cur_to = currencies.inverse_search(self.names, cur_to_id)

            if cur_from is not None and cur_to is not None:
                result.add((cur_from, cur_to))

        # print(result)
        return result

    def _exx_pair(self, cur_from, cur_to):
        return '{}_{}'.format(
            self.names[cur_from].lower(),
            self.names[cur_to].lower()
        )

    def get_pair(self, cur_from, cur_to):
        if (cur_from, cur_to) in self.available_pairs:
            return self._exx_pair(cur_from, cur_to)
        else:
            raise NoPairException(cur_from, cur_to)

    def get_depth(self, cur_from, cur_to, depth_size=5):

        pair_name = self.get_pair(cur_from, cur_to)
        url = self.BASE_URL + 'depth'
        params = {
            'currency': pair_name,
            'limit': depth_size
        }
        r = requests.get(url, params, timeout=TIMEOUT_SEC)
        r.raise_for_status()

        result = r.json()
        asks = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='ask') for r in result.get("asks", [])]
        bids = [order_book.Order(price=float(r[0]), volume=float(r[1]), type='bid') for r in result.get("bids", [])]
        return order_book.OrderBook(ask_orders=asks, bid_orders=bids)