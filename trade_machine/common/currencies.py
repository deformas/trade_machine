# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import enum

class Currency(enum.IntEnum):
    Bitcoin           = 0
    BTC               = 0

    BitcoinCash       = 1
    BCC               = 1
    
    Ethereum          = 2
    ETH               = 2

    LIZA              = 3

    ZCash             = 4
    ZEC               = 4

    Litecoin          = 5
    LTC               = 5

    EthereumClassic   = 6
    ETC               = 6

    BTCRED            = 7

    BitcoinGold       = 8
    BTG               = 8

    DASH              = 9

    BIO               = 10

    EOS               = 11

    DogeCoin          = 12
    DOGE              = 12

    WAVES             = 13

    BitcoinDiamond    = 14
    BCD               = 14

    BigGoldFishCoin   = 15
    BGF               = 15

    Lisk              = 16
    LSK               = 16

    LindaCoin         = 17
    LINDA             = 17

    NEconomyMovement  = 18
    XEM               = 18

    ECO               = 19

    YOB2X             = 20

    SibCoin           = 21
    SiberianChervon   = 21
    SIB               = 21

    NooCoin           = 22
    NOO               = 22

    Cryptonex         = 23
    CNX               = 23

    Decred            = 24
    DCR               = 24

    Vertcoin          = 25
    VTC               = 25

    CannabisCoin      = 26
    CANN              = 26

    Babel             = 27
    BAB               = 27

    RegalCoin         = 28
    REC               = 28

    OnixCoin          = 29
    ONX               = 29

    DigiByte          = 30
    DGB               = 30

    ClubCoin          = 31
    CLUB              = 31

    NoLimitCoin2      = 33
    NLC2              = 33

    BuzzCoin          = 35
    BUZZ              = 35

    GoldBlocks        = 37
    GB                = 37

    XiosCoin          = 38
    XIOS              = 38

    Incrementum       = 39
    INC               = 39

    VSLICE            = 40

    DraftCoin         = 41
    DFT               = 41

    Darknet           = 42
    PIVX              = 42

    DarkCrave         = 42
    DCC               = 43

    MavericChain      = 44
    MVC               = 44

    EthereumLite      = 46
    ELITE             = 46

    # BitfinRecRightTok = 47
    # RRT               = 47

    Monero            = 48
    XMR               = 48

    Ripple            = 50
    XRP               = 50

    Iota              = 51
    IOT               = 51

    SantimentToken    = 52
    SAN               = 52

    OmiseGO           = 53
    OMG               = 53

    NEO               = 54

    MetaverseETP      = 55
    ETP               = 55

    Quantum           = 56
    QTM               = 56

    AVT               = 57

    EidooToken        = 58
    EDO               = 58

    DAT               = 59

    QSH               = 60

    YYW               = 61

    Peercoin          = 62
    PPC               = 62

    Feathercoin       = 63
    FTC               = 63

    ReddCoin          = 64
    RDD               = 64

    NXT               = 65

    PotCoin           = 66
    POT               = 66

    Monaco            = 67
    MCO               = 67

    Cardano           = 68
    ADA               = 68

    # Lumen             = 69
    # XLM               = 69

    Einsteinium       = 70
    EMC2              = 70

    Siacoin           = 71
    SC                = 71

    MonaCoin          = 72
    MONA              = 72

    RISE              = 73

    Stratis           = 74
    STRAT             = 74

    Voxels            = 75
    VOX               = 75

    Edgeless          = 76
    EDG               = 76

    PowerLedger       = 77
    PWR               = 77

    OkCash            = 78
    OK                = 78

    TenX_Pay          = 79
    PAY               = 79

    Tether            = 80
    USDT              = 80

    Guppy             = 81
    GUP               = 81

    SysCoin           = 82
    SYS               = 82

    Civic             = 83
    CVC               = 83

    InternetOfPeople  = 84
    IOP               = 84

    QTUM              = 85

    Komodo            = 86
    KMD               = 86

    ARK               = 87

    RipioCredit       = 88
    RCN               = 88

    Ardor             = 89
    ARDR              = 89

    StatusNetworkToken= 90
    SNT               = 90

    ZCoin             = 91
    XZC               = 91

    Verge             = 92
    DogeCoinDark      = 92
    XVG               = 92

    GlobalCurReserve  = 93
    GCR               = 93

    Groestlcoin       = 94
    GRS               = 94

    SALT              = 95

    AdEx              = 96
    ADX               = 96

    NAVCoin           = 97
    NAV               = 97

    BitBay            = 98
    BAY               = 98

    BasicAttToken     = 99
    BAT               = 99

    STORJ             = 100

    Nexus             = 101
    NXS               = 101

    UnbreakableToken  = 102
    UNB               = 102

    iEx_ec            = 103
    RLC               = 103

    TransferCoin      = 104
    TX                = 104

    HempCoin          = 105
    THC               = 105

    STEEM             = 106

    Golem             = 107
    GNT               = 107

    Patientory        = 108
    PTOY              = 108

    Humaniq           = 109
    HMQ               = 109

    Metal             = 110
    MTL               = 110

    Firstblood        = 111
    _1ST              = 111

    Cofound_it        = 113
    CFI               = 113

    TrigToken         = 114
    TRIG              = 114

    Memetic           = 115
    MEME              = 115

    ZenCash           = 116
    ZEN               = 116

    IDNI_Agoras       = 117
    AGRS              = 117

    Mercury           = 118
    MER               = 118

    LBRY_Credits      = 119
    LBC               = 119

    GameCredits       = 120
    GAME              = 120

    Blocktix          = 121
    TIX               = 121

    SingularDTV       = 122
    SNGLS             = 122

    Ubiq              = 123
    UBQ               = 123

    DECENT            = 124
    DCT               = 124

    Radium            = 125
    RADS              = 125

    Nubits            = 126
    NBT               = 126

    Augur             = 127
    REP               = 127

    FunFair           = 128
    FUN               = 128

    Lomocoin          = 129
    LMC               = 129

    Factom            = 130
    FCT               = 130

    Bytes             = 131
    GBYTE             = 131

    SafeExchangeCoin  = 132
    SAFEX             = 132

    ViaCoin           = 133
    VIA               = 133

    DopeCoin          = 134
    DOPE              = 134

    Numeraire         = 135
    NMR               = 135

    CircuitsOfValue   = 136
    COVAL             = 136

    District0x        = 137
    DNT               = 137

    BlackCoin         = 138
    BLK               = 138

    RubyCoin          = 139
    RBY               = 139

    SteemDollars      = 140
    SBD               = 140

    Sphere            = 141
    SPHR              = 141

    MonetaryUnit      = 142
    MUE               = 142

    BURST             = 143

    Elastic           = 144
    XEL               = 144

    QuantumResistLed  = 145
    QRL               = 145

    Expanse           = 146
    EXP               = 146

    KORE              = 147

    CloakCoin         = 148
    CLOAK             = 148

    ION               = 149

    BitSwift          = 150
    SWIFT             = 150

    ARN               = 151
    Aeron             = 151

    MANA              = 152
    Decentraland      = 152

    BNB               = 153
    BinanceCoin       = 153

    TRON              = 154

    Stellar           = 155
    XLM               = 155

    RequestNetwork    = 156
    REQ               = 156

    BitShares         = 157
    BTS               = 157

    QuantStamp        = 158
    QSP               = 158

    Walton            = 159
    WTC               = 159

    VeChain           = 160
    VEN               = 160

    Tierion           = 161
    TNT               = 161

    Ambrosus          = 162
    AMB               = 162

    Bancor            = 163
    BNT               = 163

    Centra            = 164
    CTR               = 164

    Monetha           = 165
    MTH               = 165

    SONM              = 166

    Viberate          = 167
    VIB               = 167

    _0x               = 168
    ZRX               = 168

    Bitmark           = 169
    BTM               = 169

    VeriCoin          = 170
    VRC               = 170

    BitcoinPlus       = 171
    XBC               = 171

    BitcoinDark       = 172
    BTCD              = 172

    BitConnect        = 173

    DigitalNote       = 174
    XDN               = 174

    ErosCoin          = 175
    ERO               = 175

    Stox              = 176
    STX               = 176

    Hive              = 177
    HVN               = 177

    ByteCoin          = 178
    BCN               = 178


    ANY_CRYPTO        = 9999


    USDollar          = 10000
    USD               = 10000

    RussianRuble      = 10001
    RUB               = 10001

    Euro              = 10002
    EUR               = 10003

    ANY_FIAT          = 99999

    ANY               = 1000000


ISO_NAMES = {
    Currency.Bitcoin:         'XBT',
    Currency.BitcoinCash:     'BCH',
    Currency.Ethereum:        'ETH',
    Currency.LIZA:            'LIZA',
    Currency.ZCash:           'ZEC',
    Currency.Litecoin:        'LTC',
    Currency.ETC:             'ETC',
    Currency.BTCRED:          'BTCRED',
    Currency.BitcoinGold:     'BTG',
    Currency.DASH:            'DASH',
    Currency.BIO:             'BIO',
    Currency.EOS:             'EOS',
    Currency.DogeCoin:        'DOGE',
    Currency.WAVES:           'WAVES',
    Currency.BitcoinDiamond:  'BCD',
    Currency.BigGoldFishCoin: 'BGF',
    Currency.Lisk:            'LSK',
    Currency.DigitalNote:     'XDN',



    Currency.USDollar:       'USD',
    Currency.RussianRuble:   'RUB'
}

TRADE_NAMES = {
    Currency.Bitcoin:         'BTC',
    Currency.BitcoinCash:     'BCC',
    Currency.Ethereum:        'ETH',
    Currency.LIZA:            'LIZA',
    Currency.ZCash:           'ZEC',
    Currency.Litecoin:        'LTC',
    Currency.ETC:             'ETC',
    Currency.BTCRED:          'BTCRED',
    Currency.BitcoinGold:     'BTG',
    Currency.DASH:            'DASH',
    Currency.BIO:             'BIO',
    Currency.EOS:             'EOS',
    Currency.DogeCoin:        'DOGE',
    Currency.WAVES:           'WAVES',
    Currency.BitcoinDiamond:  'BCD',
    Currency.BigGoldFishCoin: 'BGF',
    Currency.Lisk:            'LSK',
    Currency.LindaCoin:       'LINDA',
    Currency.XEM:             'XEM',
    Currency.ECO:             'ECO',
    Currency.YOB2X:           'YOB2X',
    Currency.SIB:             'SIB',
    Currency.NooCoin:         'NOO',
    Currency.Cryptonex:       'CNX',
    Currency.Decred:          'DCR',
    Currency.Vertcoin:        'VTC',
    Currency.CannabisCoin:    'CANN',
    Currency.Babel:           'BAB',
    Currency.RegalCoin:       'REC',
    Currency.OnixCoin:        'ONX',
    Currency.DigiByte:        'DGB',
    Currency.ClubCoin:        'CLUB',
    Currency.NoLimitCoin2:    'NLC2',
    Currency.BuzzCoin:        'BUZZ',
    Currency.GoldBlocks:      'GB',
    Currency.XiosCoin:        'XIOS',
    Currency.Incrementum:     'INC',
    Currency.VSLICE:          'VSLICE',
    Currency.DraftCoin:       'DFT',
    Currency.Darknet:         'PIVX',
    Currency.DarkCrave:       'DCC',
    Currency.MavericChain:    'MVC',
    Currency.EthereumLite:    'ELITE',
    Currency.Monero:          'XMR',
    Currency.Ripple:          'XRP',
    Currency.Iota:            'IOT',
    Currency.SantimentToken:  'SAN',
    Currency.OmiseGO:         'OMG',
    Currency.NEO:             'NEO',
    Currency.EidooToken:      'ETP',
    Currency.Quantum:         'QTM',
    Currency.AVT:             'AVT',
    Currency.EidooToken:      'EDO',
    Currency.DAT:             'DAT',
    Currency.QSH:             'QSH',
    Currency.YYW:             'YYW',
    Currency.Peercoin:        'PPC',
    Currency.Feathercoin:     'FTC',
    Currency.ReddCoin:        'REDD',
    Currency.NXT:             'NXT',
    Currency.PotCoin:         'POT',
    Currency.Monaco:          'MCO',
    Currency.ADA:             'ADA',
    # Currency.Lumen:           'XLM',
    Currency.Einsteinium:     'EMC2',
    Currency.Siacoin:         'SC',
    Currency.MonaCoin:        'MONA',
    Currency.RISE:            'RISE',
    Currency.Stratis:         'STRAT',
    Currency.Voxels:          'VOX',
    Currency.Edgeless:        'EDG',
    Currency.PowerLedger:     'PWR',
    Currency.OkCash:          'OK',
    Currency.TenX_Pay:        'PAY',
    Currency.Tether:          'USDT',
    Currency.Guppy:           'GUP',
    Currency.SysCoin:         'SysCoin',
    Currency.Civic:           'CVC',
    Currency.InternetOfPeople:'IOP',
    Currency.QTUM:            'QTUM',
    Currency.Komodo:          'KMD',
    Currency.ARK:             'ARK',
    Currency.RipioCredit:     'RCN',
    Currency.Ardor:           'ARDR',
    Currency.StatusNetworkToken: 'SNT',
    Currency.ZCoin:           'XZC',
    Currency.Verge:           'XVG',
    Currency.GlobalCurReserve: 'GCR',
    Currency.Groestlcoin:     'GRS',
    Currency.SALT:            'SALT',
    Currency.AdEx:            'ADX',
    Currency.NAV:             'NAVCoin',
    Currency.BitBay:          'BAY',
    Currency.BasicAttToken:   'BAT',
    Currency.STORJ:           'STORJ',
    Currency.Nexus:           'NXS',
    Currency.UnbreakableToken: 'UNB',
    Currency.iEx_ec:          'RLC',
    Currency.TransferCoin:    'TX',
    Currency.HempCoin:        'THC',
    Currency.STEEM:           'STEEM',
    Currency.Golem:           'GNT',
    Currency.Patientory:      'PTOY',
    Currency.Humaniq:         'HMQ',
    Currency.Metal:           'MTL',
    Currency.Firstblood:      '1ST',
    Currency.Cofound_it:      'Cofound_it',
    Currency.TrigToken:       'TRIG',
    Currency.Memetic:         'MEME',
    Currency.ZenCash:         'ZEN',
    Currency.IDNI_Agoras:     'AGRS',
    Currency.Mercury:         'MER',
    Currency.LBRY_Credits:    'LBC',
    Currency.GameCredits:     'GAME',
    Currency.Blocktix:        'TIX',
    Currency.SingularDTV:     'SNGLS',
    Currency.Ubiq:            'UBQ',
    Currency.DECENT:          'DCT',
    Currency.Radium:          'RADS',
    Currency.Nubits:          'NBT',
    Currency.Augur:           'REP',
    Currency.FunFair:         'FUN',
    Currency.Lomocoin:        'LMC',
    Currency.Factom:          'FCT',
    Currency.Bytes:           'GBYTE',
    Currency.SafeExchangeCoin:'SAFEX',
    Currency.ViaCoin:         'VIA',
    Currency.DopeCoin:        'DOPE',
    Currency.Numeraire:       'NMR',
    Currency.CircuitsOfValue: 'COVAL',
    Currency.District0x:      'DNT',
    Currency.BlackCoin:       'BLK',
    Currency.RubyCoin:        'RBY',
    Currency.SteemDollars:    'SBD',
    Currency.Sphere:          'SPHR',
    Currency.MonetaryUnit:    'MUE',
    Currency.BURST:           'BURST',
    Currency.Elastic:         'XEL',
    Currency.QuantumResistLed:'QRL',
    Currency.Expanse:         'EXP',
    Currency.KORE:            'KORE',
    Currency.CloakCoin:       'CLOAK',
    Currency.ION:             'ION',
    Currency.BitSwift:        'SWIFT',
    Currency.Aeron:           'ARN',
    Currency.Decentraland:    'MANA',
    Currency.BinanceCoin:     'BNB',
    Currency.TRON:            'TRX',
    Currency.Stellar:         'XLM',
    Currency.RequestNetwork:  'REQ',
    Currency.BitShares:       'BTS',
    Currency.QuantStamp:      'QSP',
    Currency.Walton:          'WTC',
    Currency.VeChain:         'VEN',
    Currency.Tierion:         'TNT',
    Currency.Ambrosus:        'AMB',
    Currency.Bancor:          'BNT',
    Currency.Centra:          'CTR',
    Currency.Monetha:         'MTH',
    Currency.SONM:            'SONM',
    Currency.Viberate:        'VIB',
    Currency._0x:             'ZRX',
    Currency.Bitmark:         'BTM',
    Currency.VeriCoin:        'VRC',
    Currency.BitcoinPlus:     'XBC',
    Currency.BitcoinDark:     'BTCD',
    Currency.BitConnect:      'BitConnect',
    Currency.DigitalNote:     'XDN',
    Currency.ErosCoin:        'ERO',
    Currency.Stox:            'STX',
    Currency.Hive:            'HVN',
    Currency.ByteCoin:        'BCN',



    Currency.USDollar:       'USD',
    Currency.RussianRuble:   'RUR',
    Currency.Euro:           'EUR'
}

def inverse_search(d, value):
    for k, v in d.items():
        if value.upper() == v:
            return k

    return None

def is_fiat(cur):
    return cur >= 1000