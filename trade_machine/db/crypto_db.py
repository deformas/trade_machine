# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import pymongo

class OHLCFreq:
    DAILY = 'daily'
    HOURLY = 'hourly'
    MINUTELY = 'minutely'

class CryptoDB(object):
    def __init__(self):
        self.client = pymongo.MongoClient()

    def _col_name(self, cur_from, cur_to, freq):
        return '{fr}_{to}_{freq}'.format(fr=cur_from, to=cur_to, freq=freq)

    def add_ohlc_data(self, source, cur_from, cur_to, freq, data):
        db = self.client[source]
        collection = db[self._col_name(cur_from, cur_to, freq)]

        if collection.count() == 0:
            collection.create_index([('time', pymongo.ASCENDING)], unique=True)

        try:
            collection.insert_many(data, ordered=False)
        except pymongo.errors.BulkWriteError as e:
            print('Errors while adding: {}'.format(len(e.details['writeErrors'])))

    def get_last_ohlc_update(self, source, cur_from, cur_to, freq):
        db = self.client[source]
        collection = db[self._col_name(cur_from, cur_to, freq)]

        if collection.count() == 0:
            return None

        return collection.find_one(sort=[("time", -1)])['time']

    def get_size(self, source, cur_from, cur_to, freq):
        db = self.client[source]
        collection = db[self._col_name(cur_from, cur_to, freq)]

        return collection.count()


    # def update_fobd_data(self, source, cur_from, cur_to, data):
