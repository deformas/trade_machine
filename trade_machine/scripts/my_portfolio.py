import requests

def main():
    values = [
        ('btc_rur', 0.002764, 2998.94),
        ('eth_rur', 0.071513, 2999.97035),
        ('etc_rur', 1.41, 3076.62),
        ('bcc_rur', 0.02739726, 2999.99997),
        ('zec_rur', 0.10732, 2997.9842)
    ]

    total_diff, total = 0, 0
    print('Ticker    \tCur Value\t Change (%)  \tPrice    ')
    for v in values:
        data = requests.get('https://yobit.net/api/3/ticker/' + v[0]).json()[v[0]]
        last = data['last']
        print(v[0], '\t', '{:.2f}'.format(last * v[1]), '\t', '{:+.2f}%'.format(v[1] * last / v[2] * 100 - 100), '\t', '{:0f}'.format(last))

        total += v[2]
        total_diff += v[1] * last

    print('Was {:2f} RUB now {:2f} RUB (change {:.2f}%)'.format(total, total_diff, total_diff / total * 100 - 100))

if __name__ == '__main__':
    main()