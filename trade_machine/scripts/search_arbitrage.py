# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import gevent
import time
import sys

from trade_machine.common.exchanges import YobitExchange, BitfinexExchange, BittrexExchange, \
    BinanceExchange, PoloniexExchange, HitBTCExchange, LiquiExchange, EXXExchange
from trade_machine.common.exchanges import NoPairException

from trade_machine.common.currencies import Currency, TRADE_NAMES
from trade_machine.common.order_book import Order

HOLD_PAIR_SEC = 300
SLEEP_FOR_EXCHANGE_SEC = 5

class Balance(object):
    def __init__(self, currency, amount):
        self.currency = currency
        self.amount = amount


class OnlineExchange(gevent.Greenlet):
    def __init__(self):
        gevent.Greenlet.__init__(self)

    def listen(self):
        """Return new order book
        """
        while True:
            self.update_order_books()
            gevent.sleep(SLEEP_FOR_EXCHANGE_SEC)

    def update_order_books(self):
        pass

class ExchangeSet(object):
    """Store all available exchanges and check it for arbitrage after every update
    """
    def __init__(self):
        pass

    def add_exchange(self, exchange):
        pass

    def listen(self, event_type):
        pass

    def _is_arbitrage_exists(self):
        pass

# import sys, traceback

def get_order_book(exchange, pair, depth=1):
    try:
        return exchange.get_depth(pair[0], pair[1], depth_size=1), False
    except NoPairException as ex:
        # print(ex)
        pass
    except Exception as ex:
        raise ex
        print(ex, file=sys.stderr)

    try:
        return exchange.get_depth(pair[1], pair[0], depth_size=1), True
    except NoPairException as ex:
        # print(ex)
        pass
    except Exception as ex:
        raise ex
        print(ex, file=sys.stderr)

    print(pair, exchange.get_name())
    raise Exception("Strange, but pair is not in this castle..")


def get_simple_arbitrage():
    exchanges_list = []
    exchanges_list.append(YobitExchange(TRADE_NAMES))
    exchanges_list.append(BitfinexExchange(TRADE_NAMES))
    exchanges_list.append(BittrexExchange(TRADE_NAMES))
    exchanges_list.append(BinanceExchange(TRADE_NAMES))
    exchanges_list.append(PoloniexExchange(TRADE_NAMES))
    exchanges_list.append(HitBTCExchange(TRADE_NAMES))
    exchanges_list.append(LiquiExchange(TRADE_NAMES))
    exchanges_list.append(EXXExchange(TRADE_NAMES))

    mutual_pairs = dict()

    for i in range(len(exchanges_list)):
        # res1 = (Currency.ETH, Currency.BTC) in exchanges_list[i].available_pairs
        # res2 = (Currency.BTC, Currency.ETH) in exchanges_list[i].available_pairs
        # print(res1, res2)

        for j in range(i + 1, len(exchanges_list)):
            pairs1 = [((i, j) if i < j else (j, i)) for i, j in list(exchanges_list[i].available_pairs)]
            pairs2 = [((i, j) if i < j else (j, i)) for i, j in list(exchanges_list[j].available_pairs)]
            intersections = list(set(pairs1) & set(pairs2))
            print('Find {} intersections between {} and {}'.format(
                len(intersections),
                exchanges_list[i].get_name(),
                exchanges_list[j].get_name()
            ))
            for k in range(len(intersections)):
                print('{}: {}'.format(
                    k,
                    intersections[k]
                ))
                if intersections[k] not in mutual_pairs:
                    mutual_pairs[intersections[k]] = set()
                mutual_pairs[intersections[k]].add(i)
                mutual_pairs[intersections[k]].add(j)

    for k in mutual_pairs.keys():
        mutual_pairs[k] = list(mutual_pairs[k])

    for pair in mutual_pairs.keys():
        min_ask = Order(10000000.0, 0, Order.ASK)
        max_bid = Order(0, 0, Order.BID)

        ask_exchange_num = None
        bid_exchange_num = None

        for ex_number in mutual_pairs[pair]:
            ob, reverse = get_order_book(exchanges_list[ex_number], pair)
            if pair[0] in exchanges_list[ex_number].NOT_WITHDRAWABLE or pair[1] in exchanges_list[ex_number].NOT_WITHDRAWABLE:
                continue
            top_asks = ob.top_n(1, Order.ASK)
            # if len(top_asks) > 0:
            #     print(top_asks[0].price, exchanges_list[ex_number].get_name())

            # if reverse and len(top_bids) == 1:
            #     top_asks[0].price = 1 / top_asks[0].price

            if len(top_asks) == 1 and top_asks[0].price < min_ask.price and top_asks[0].volume > 0:
                min_ask = top_asks[0]
                ask_exchange_num = ex_number

            top_bids = ob.top_n(1, Order.BID)
            # if len(top_bids) > 0:
            #     print(top_bids[0].price, exchanges_list[ex_number].get_name())
            # if reverse and len(top_bids) == 1:
            #     top_bids[0].price = 1 / top_bids[0].price
            if len(top_bids) == 1 and top_bids[0].price > max_bid.price and top_bids[0].volume > 0:
                max_bid = top_bids[0]
                bid_exchange_num = ex_number

        if ask_exchange_num is None or bid_exchange_num is None:
            continue
        else:
            bid_exchange_name = exchanges_list[bid_exchange_num].get_name()
            ask_exchange_name = exchanges_list[ask_exchange_num].get_name()

        spread = (min_ask.price - max_bid.price) / ((min_ask.price + max_bid.price) / 2)
        if spread > -0.01:
            continue

        print('Pair: {} / {}. Max bid is {:.8f} on {}, min ask is {:.8f} on {}. Spread is {:.4f}%'.format(
            TRADE_NAMES[pair[0]],
            TRADE_NAMES[pair[1]],
            max_bid.price,
            bid_exchange_name,
            min_ask.price,
            ask_exchange_name,
            spread * 100
        ))
        print('\n')

        time.sleep(1)

if __name__ == '__main__':
    get_simple_arbitrage()

