# coding: utf-8

import gevent
from gevent import Greenlet

class MyGreenlet(Greenlet):

    def __init__(self, message, n):
        Greenlet.__init__(self)
        self.message = message
        self.n = n

    def _run(self):
        print(self.message)
        gevent.sleep(self.n)
        self.n = 10

g = MyGreenlet("Hi there!", 3)
g.start()
print(g.n)
print(g.ready())
# g.join()
gevent.sleep(5)
print(g.n)
print(g.ready())
