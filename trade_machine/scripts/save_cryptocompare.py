# Copyright 2017 The Fluke Fund . All Rights Reserved.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: deformas

import datetime

from trade_machine.scrapper import data_source
from trade_machine.db import crypto_db

def main():
    api = data_source.DataSource.get_api(data_source.DataSource.Cryptocompare)
    db = crypto_db.CryptoDB()

    for freq in api.DataFrequency:
        for exchange in api.ExchangeList:
            print('Start {freq} {exchange}'.format(freq=freq, exchange=exchange))

            start_date = db.get_last_ohlc_update(exchange, 'btc', 'usd', freq)
            if start_date is None:
                if freq == api.FreqHourly:
                    start_date = datetime.datetime.now() - datetime.timedelta(days=365)
                else:
                    start_date = datetime.datetime.now() - datetime.timedelta(days=7)

            data = api.get_currency_history(
                cur_from='BTC',
                cur_to='USD',
                exchange=exchange,
                freq=freq,
                dt_from=start_date
            )

            data = [i.to_db() for i in list(data.values())]
            db.add_ohlc_data(exchange, 'btc', 'usd', freq, data)

            print('New size: {}'.format(db.get_size(exchange, 'btc', 'usd', freq)))

if __name__ == '__main__':
    main()